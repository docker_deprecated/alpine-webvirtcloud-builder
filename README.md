# alpine-webvirtcloud-builder

#### [alpine-x64-webvirtcloud-builder](https://hub.docker.com/r/forumi0721alpinex64build/alpine-x64-webvirtcloud-builder/)
![Docker Image Version (tag latest semver)](https://img.shields.io/docker/v/forumi0721alpinex64build/alpine-x64-webvirtcloud-builder/latest)
![Docker Image Size (tag)](https://img.shields.io/docker/image-size/forumi0721alpinex64build/alpine-x64-webvirtcloud-builder/latest)
![MicroBadger Layers (tag)](https://img.shields.io/microbadger/layers/forumi0721alpinex64build/alpine-x64-webvirtcloud-builder/latest)
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpinex64build/alpine-x64-webvirtcloud-builder)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpinex64build/alpine-x64-webvirtcloud-builder)



----------------------------------------
#### Description

* Distribution : [Alpine Linux](https://alpinelinux.org/)
* Architecture : x64
* Appplication : [WebVirtCloud](https://github.com/retspen/webvirtcloud) (Build Dependencies)
    - WebVirtCloud is virtualization web interface for admins and users.



----------------------------------------
#### Run

* Nothing



----------------------------------------
#### Usage

* Nothing



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Environment Variables

| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |

